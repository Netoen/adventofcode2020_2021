import logging
from glob import glob
import re

logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

assert __name__ == "__main__", "You are not running the main file "


def read_input(sep: str = '\n') -> list:
    with open(glob('*.input')[0]) as f:
        input_data = f.read()
    del f
    return input_data.split(sep)


def count_bags(color: str):
    if items[color][0][1] == 'other':
        return 1
    else:
        return sum([count_bags(sub_bag[1])*int(sub_bag[0]) for sub_bag in items[color]])+1

items = {}
items_raw = read_input('\n')
items_raw = [re.sub(r'( bags\.| bag\.| bags| bag|\.)', '', item) for item in items_raw]
for item in items_raw:
    raw_line=item.split(' contain ')
    sub_bags=raw_line[1].split(', ')
    for sub_bag in sub_bags:
        index=sub_bags.index(sub_bag)
        sub_bags[index]=re.split(r'\s', sub_bag, 1)
    items[raw_line[0]]=sub_bags

del items_raw
logging.debug(f'Currently list of seats looks like this:\n{items}')

solution=count_bags('shiny gold') - 1
logging.info(f'The result is {solution}')