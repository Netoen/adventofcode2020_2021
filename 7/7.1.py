import logging
from glob import glob
from dataclasses import dataclass

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

assert __name__ == "__main__", "You are not running the main file "


def read_input(sep: str = '\n') -> list:
    with open(glob('*.input')[0]) as f:
        input_data = f.read()
    del f
    return input_data.split(sep)


@dataclass
class bag_definition():
    p_color: str
    c_colors: list


def find_parent(color: str):
    for item in items_raw:
        if (color in item[1:]) and (items[items_raw.index(item)].p_color not in solution):
            new_color = items[items_raw.index(item)].p_color
            solution.add(new_color)
            find_parent(new_color)


items = []
items_raw = read_input('\n')
for item in items_raw:
    raw_line = item.split(' bags contain ')
    items.append(bag_definition(raw_line[0], raw_line[1].split(', ')))
# del items_raw
logging.debug(f'Currently list of seats looks like this:\n{items}')


solution = set()
find_parent('shiny gold')
logging.debug(f'The resulting set is {solution}')
logging.info(f'And the summ is {len(solution)}')