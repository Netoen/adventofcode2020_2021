import logging
from glob import glob
from math import ceil

logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


class a_seat:
    row_raw: str
    row: int = None
    col_raw: str
    col: int = None
    id = None

    def __init__(self, row_raw: str, col_raw: str):
        self.row_raw = row_raw
        self.col_raw = col_raw

    def __str__(self):
        return self.row_raw + self.col_raw

    def __repr__(self):
        return self.__str__()

    def decide(self, input: str, val_low, val_high: int, indicator_low, indicator_high: str) -> int:
        for argument in locals():
            assert argument, f'Argument {argument} in function "decide" found to be None, unacceptable!'
        cur_range = None
        for char in input:
            cur_range = val_high - val_low
            if char == indicator_high:
                val_low = ceil(cur_range / 2) + val_low
            elif char == indicator_low:
                val_high = val_high - ceil(cur_range / 2)
        assert val_low == val_high, f'Something went wrong during the "decide" function, values didn\'t converge\nval_low {val_low}, val_high {val_high}'
        return val_low


assert __name__ == "__main__", "You are not running the main file "

with open(glob('*.input')[0]) as f:
    input_data = f.read()
del f
seats = []
seats_raw = input_data.split('\n')
del input_data
for col_raw in seats_raw:
    seat = a_seat(col_raw[:7], col_raw[-3:])
    seats.append(seat)
logging.debug(f'Currently list of seats looks like this:\n{seats}')

max = 0
for seat in seats:
    seat.row = seat.decide(seat.row_raw, 0, 127, "F", "B")
    seat.col = seat.decide(seat.col_raw, 0, 7, "L", "R")
    seat.id = seat.row * 8 + seat.col
    max = max if max > seat.id else seat.id
    logging.info(f'row {seat.row}, column {seat.col}, seat ID {seat.id}')
logging.info(f'Max seat.id is {max}')
