from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path
PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
from pprint import pformat as pf
from re import sub, search
from lib import loader

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

CONST_PATIENCE = 10000


def main(**kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    input_raw = loader.read_input(input_filename_puzzle, '\n')
    solutions = loader.read_input(input_filename_solutions, '\n')

    rplc_str(input_raw)
    logging.info(pf(sum(input_raw)))
    open(".tmp_myoutput", "w").writelines('\n'.join(map(str, input_raw)))
    assert sum(input_raw) not in (29621733884803,), 'GOT THE SAME RESULT!'
    return input_raw, solutions


def rplc_str(s: list):
    for idx, line in enumerate(s):
        line = sub(r'\s+', '', line)
        line = line.replace(line, no_parenthesis(line))
        s[idx] = int(calc_it(line))


def calc_it(s: str):
    s = scan_and_replace(r'\d+\+\d+', s)  # perform all the summations
    s = scan_and_replace(r'\d+\*\d+', s)  # perform all the multiplications
    s = sub(r'(?:\(|\))', '', s)
    return s


def scan_and_replace(r, s):
    watchdog = 0
    while (pluses := search(r, s)) and watchdog < CONST_PATIENCE:
        watchdog += 1
        _ = pluses[0]
        s = s.replace(_, str(eval(_)), 1)
    assert watchdog < CONST_PATIENCE, 'WATCHDOG IS UNHAPPY'
    return s


def no_parenthesis(s: str) -> str:
    lps, grps = [[] for _ in range(2)]
    if '(' in s:
        for idx, c in enumerate(s):
            if c == '(':
                lps.append(idx)
            elif c == ')':
                if len(lps) == 1:
                    if lps[0] == 0 and idx == len(s) - 1:
                        s = s[1:-1]
                        s = s.replace(s, no_parenthesis(s),1)
                    else:
                        grps.append(s[lps.pop(): idx + 1])
                else:
                    lps.pop()
    for pp in grps:
        s = s.replace(pp, no_parenthesis(pp), 1)
    return calc_it(s)


# assert __name__ == "__main__", "You are not running the main file "

main()