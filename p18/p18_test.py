import unittest
import p18_1, p18_2


class test_my_case(unittest.TestCase):

    def test_example_1(self):
        my_input, solutions = p18_1.main(input_filename_puzzle='*_1.e', input_filename_solutions='*_1.s')
        for i in range(len(my_input)):
            self.assertEqual(int(my_input[i]), int(solutions[i]))

    def test_example_2(self):
        my_input, solutions = p18_2.main(input_filename_puzzle='*_1.e', input_filename_solutions='*_2.s')
        for i in range(len(my_input)):
            self.assertEqual(int(my_input[i]), int(solutions[i]))


if __name__ == "__main__":
    unittest.main()
