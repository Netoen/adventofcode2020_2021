import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
import logging
from lib import loader
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

# CONST
CONST_PREAMBLE = 25

assert __name__ == "__main__", "You are not running the main file "

input_raw = loader.read_input('\n')
input = [int(item) for item in input_raw]

def is_faulty(v: int, l: list):
    for item in l:
        if v - item in l and l.index(v-item) != l.index(item):
            return False
    return True

for item in input[CONST_PREAMBLE:]:
    if is_faulty(item, input[input.index(item)-CONST_PREAMBLE:input.index(item)]):
        sub_solution = item

solution = None
for i in range(len(input)):
    j = i
    cur_summ = input[j]
    while cur_summ < sub_solution:
        j += 1
        cur_summ += input[j]
    assert j < sys.maxsize-1, 'We ran out of bounds on the j'
    if cur_summ == sub_solution and j-i>=1:
        solution = min(input[i : j+1]) + max(input[i : j+1])
        break

logging.debug(input)
if solution: logging.info(solution)
else: logging.error('FOUND NO SOLUTION')