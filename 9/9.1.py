import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
import logging
from lib import loader
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

# CONST
CONST_PREAMBLE = 25

assert __name__ == "__main__", "You are not running the main file "

input_raw = loader.read_input('\n')
input = [int(item) for item in input_raw]

def is_faulty(v: int, l: list):
    for item in l:
        if v - item in l and l.index(v-item) != l.index(item):
            return False
    return True

for item in input[CONST_PREAMBLE:]:
    if is_faulty(item, input[input.index(item)-CONST_PREAMBLE:input.index(item)]):
        solution = item

logging.debug(input)
logging.info(solution)