from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
from lib import loader
from re import match

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


def main(**kwargs):
    puzzle = get_puzzle(kwargs)
    solutions = get_solutions(kwargs)

    ruleset, puzzle = extract_rules(puzzle)
    my_re = enforce_rule(ruleset, ruleset[0])
    result = count_lines(my_re, puzzle)

    if __name__ != '__main__':
        return result, solutions  # FIXME: return proper items for the tests
    else:
        logging.info(result)


def count_lines(my_re, puzzle):
    cnt = 0
    for line in puzzle:
        if match('^' + my_re + '$', line):
            cnt += 1
    return cnt


def enforce_rule(ruleset: dict, rule: str):
    if rule not in ('a', 'b'):
        if '|' in rule:
            return f"(?:{'|'.join([enforce_rule(ruleset, rule) for rule in rule.split('|')])})"
        else:
            return f"{''.join([enforce_rule(ruleset, ruleset[int(_)]) for _ in rule.strip().split(' ')])}"  # (?:)
    else:
        return rule


def extract_rules(puzzle):
    result = dict()
    for idx, line in enumerate(puzzle):
        if not (_ := match(r'^(\d+):\s(.*)$', line)):
            puzzle = puzzle[idx+1:]
            break
        result[int(_[1])] = _[2][1:-1] if match(r'"\w"', _[2]) else _[2]
    return result, puzzle


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()
