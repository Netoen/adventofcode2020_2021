from init import *
from re import match, finditer

CONST_RULE_8_NEW = '42 (?:8)?'
CONST_RULE_8_OLD = '42'
CONST_RULE_11_NEW = '42 (?:11)? 31'
CONST_RULE_11_OLD = '42 31'

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


def main(**kwargs):
    puzzle = get_puzzle(kwargs)
    solutions = get_solutions(kwargs)

    ruleset, puzzle = extract_rules(puzzle)
    my_re = enforce_rule(ruleset, rule_idx=0, revs=0).replace(" ", "")
    result = count_lines(my_re, puzzle)

    if __name__ != '__main__':
        return result, solutions
    else:
        logging.info(result)


def count_lines(my_re, puzzle):
    cnt = 0
    for line in puzzle:
        if match('^' + my_re + '$', line):
            cnt += 1
    return cnt


def enforce_rule(ruleset: dict, rule_idx: int, revs: int):
    rule = ruleset[rule_idx]
    if rule not in ('a', 'b'):
        if '|' in rule:
            return f"(?:{'|'.join([substitute_subrule(ruleset, _, revs) for _ in rule.split('|')])})"
        else:
            if CONST_RULE_8_NEW in rule or CONST_RULE_11_NEW in rule:
                revs += 1
            if revs >= 5:
                rule = rule.replace(CONST_RULE_8_NEW, CONST_RULE_8_OLD).replace(CONST_RULE_11_NEW, CONST_RULE_11_OLD)
            return substitute_subrule(ruleset, rule, revs)
    else:
        return rule


def substitute_subrule(ruleset, subrule, revs):
    for number in finditer(r"\d+", subrule):
        subrule = subrule.replace(number.group(), enforce_rule(ruleset, int(number.group()), revs), 1)
    return subrule


def extract_rules(puzzle):
    result = dict()
    for idx, line in enumerate(puzzle):
        if not (_ := match(r'^(\d+):\s(.*)$', line)):
            puzzle = puzzle[idx+1:]
            break
        result[int(_[1])] = _[2][1:-1] if match(r'"\w"', _[2]) else _[2]
    result[8] = CONST_RULE_8_NEW
    result[11] = CONST_RULE_11_NEW
    return result, puzzle


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_2.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()
