import unittest
import p19_1 as p1
# import p19_2 as p2


CONST_ERROR = "We failed at the {i} index, where {first} wasn't equal to {second}"


class test_my_case(unittest.TestCase):

    def test_example_1(self):
        for i in range(7):
            my_input, solutions = p1.main(idx=i, input_filename_puzzle='*_1.e', input_filename_solutions='*_1.s')
            self.assertEqual(int(solutions), int(my_input), msg=CONST_ERROR.format(i=i, first=my_input,
                                                                                   second=solutions))

    def test_example_2(self):
        for i in range(7):
            my_input, solutions = p2.main(idx=i, input_filename_puzzle='*_1.e', input_filename_solutions='*_2.s')
            self.assertEqual(int(solutions), int(my_input), msg=CONST_ERROR.format(i=i, first=my_input,
                                                                                   second=solutions))


if __name__ == "__main__":
    unittest.main()
