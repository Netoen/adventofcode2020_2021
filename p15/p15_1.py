from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
import numpy as np
from lib import loader

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

CONST_LEN = 2020


def main(**kwargs):
    puzzle = np.array(get_puzzle(kwargs)[kwargs['idx'] if 'idx' in kwargs else 0].split(','))
    solutions = get_solutions(kwargs)[kwargs['idx'] if 'idx' in kwargs else 0]

    puzzle = np.append(puzzle, np.full(CONST_LEN - (l := len(puzzle)), None))
    for turn in range(l, CONST_LEN):
        if puzzle[turn-1] in puzzle[:-1]:
            _ = np.where(puzzle == puzzle[-1])[0][-2:]
            puzzle[turn] = _[1] - _[0]
        else:
            puzzle[turn] = 0

    if __name__ != '__main__':
        return puzzle[-1], solutions
    else:
        logging.info(f"Solution is: {puzzle[-1]}")


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()
