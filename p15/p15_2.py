from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path, getsizeof

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
from tqdm import trange
from lib import loader
from time import perf_counter

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


CONST_LEN = 30000000


def main(**kwargs):
    puzzle = get_puzzle(kwargs)[kwargs['idx'] if 'idx' in kwargs else 0].split(',')
    solutions = get_solutions(kwargs)[kwargs['idx'] if 'idx' in kwargs else 0]

    last_item = int(puzzle[-1])
    puzzle = {int(item): idx for idx, item in enumerate(puzzle)}
    for turn in trange(len(puzzle), CONST_LEN):
        if puzzle[last_item] == turn - 1:
            new_item = 0
        else:
            new_item = turn - 1 - puzzle[last_item]
            puzzle[last_item] = turn - 1
        if new_item not in puzzle.keys():
            puzzle[new_item] = turn
        last_item = new_item

    if __name__ != '__main__':
        return last_item, solutions
    else:
        logging.info(f"Solution is: {last_item}")


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()