from dataclasses import dataclass
from glob import glob
import logging
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s -  %(levelname)s -  %(message)s')


with open(glob('*.input')[0]) as f:
    input_data = f.read().splitlines()
del f
total = 1
for advancements in ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2)):
    trees = 0
    cur_x = cur_y = 0
    path = ''
    while cur_y <= len(input_data) - advancements[1] - 1:
        cur_x += advancements[0]
        cur_y += advancements[1]
        path += input_data[cur_y][cur_x % len(input_data[cur_y])]
        if input_data[cur_y][cur_x % len(input_data[cur_y])] == '#':
            trees += 1
    logging.debug(f'Path so far {path}')
    logging.debug(f'Current number of trees at the end of the cycle {advancements} is {trees}')
    total *= trees
    logging.debug(f'Resulting in overall total {total}')
