from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
from lib import loader
import re
import numpy as np
import itertools as itto

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


def main(**kwargs):
    if __name__ != '__main__':
        if 'input_filename_puzzle' not in kwargs:
            return
    puzzle = get_puzzle(kwargs)
    solutions = get_solutions(kwargs)
    _ = re.finditer((r_idx := r'\[(\d+)\]'), ''.join(puzzle[1:]))
    _mem = {}
    for line in puzzle:
        if 'mask' in line:
            mask = re.search(r'[X,0,1]+', line).group()
            mask_digits = int(mask.replace('X', '0'), 2)
            mask_Xs = mask.count('X')
            mask_len = str(len(mask))
            replacements = [item.start() for item in re.finditer(r'X', mask)]
        else:
            pholder = int(re.search(r_idx, line).group(1))
            pholder = list(format(pholder | mask_digits, '0' + mask_len + 'b'))
            for X in replacements:
                pholder[X] = 'X'
            pholder = ''.join(pholder)
            products = []
            for ind in itto.product('01', repeat=mask_Xs):
                ind = list(ind)
                products.append(int("".join([char if char != 'X' else ind.pop(0) for char in pholder]), base=2))
            for idx in products:
                _mem[idx] = int(re.search(r'= (\d+)', line).group(1))
    solution = sum(_mem.values())

    if __name__ != '__main__':
        return solution, solutions[0]
    else:
        logging.info(solution)


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()
