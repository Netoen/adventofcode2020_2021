from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
from lib import loader
import re
import numpy as np

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


def main(**kwargs):
    if __name__ != '__main__':
        if 'input_filename_puzzle' not in kwargs:
            return
    puzzle = get_puzzle(kwargs)
    solutions = get_solutions(kwargs)
    _ = re.finditer((r_idx := r'\[(\d+)\]'), ''.join(puzzle[1:]))
    arr_len = max([int(item.group(1)) for item in _]) + 1
    _mem = np.zeros(arr_len, dtype=np.int64)
    for line in puzzle:
        if 'mask' in line:
            mask = re.search(r'[X,0,1]+', line).group()
            mask_len = str(len(mask))
            replaces = {item.start(): item.group() for item in re.finditer(r'[0,1]', mask)}.items()
        else:
            _ = list(format(int(re.search(r'= (\d+)', line).group(1)), '0' + mask_len + 'b'))
            for idx, item in replaces:
                _[idx] = item
                # pass
            _mem[int(re.search(r_idx, line).group(1))] = int(''.join(_), 2)
    solution = sum(_mem)
    _ = thousandify(solution)
    _mem = [thousandify(item) for item in _mem if item != 0]
    assert solution not in (9610066024360, ), 'Already guessed this number!'

    if __name__ != '__main__':
        return solution, solutions[0]
    else:
        logging.info(solution)


def thousandify(solution):
    _ = []
    s = str(solution)[::-1]
    f = len(s) // 3
    for i in range(f):
        _.append(s[:3])
        s = s[3:]
    _.append(s)
    return "`".join(_)[::-1]


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()
