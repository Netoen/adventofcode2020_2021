from dataclasses import dataclass
from glob import glob
import logging
import re
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s -  %(levelname)s -  %(message)s')


class APASSPORT:
    byr: int = None  # (Birth Year)
    iyr: int = None  # (Issue Year)
    eyr: int = None  # (Expiration Year)
    hgt: int = None  # (Height)
    hcl: str = None  # (Hair Color)
    ecl: str = None  # (Eye Color)
    pid: int = None  # (Passport ID)
    cid: int = None  # (Country ID)
    ln: int = 0      # Number of attributes being non-None

    @staticmethod
    def check_attr(attr):
        if not hasattr(APASSPORT, attr):
            raise Exception(f'We tried accessing an attribute {attr} that is not present in current passport self')
        return

    @staticmethod
    def regex_from_attr(attr):
        APASSPORT.check_attr(attr)
        return attr + r':(.*?)(?:\n|\s|$)'

    def __iter__(self):  # initialize iterable
        for attr in dir(self):
            if not (attr.startswith('_') or callable(getattr(self, attr))):
                yield attr

    def set_value(self, attr, value):
        self.check_attr(attr)
        tmp = ''
        for char in value:
            if char.isdigit():
                tmp += char
        val_i = int(tmp) if tmp != '' else 0
        if (attr == 'byr' and ((val_i < 1920 or val_i > 2002) or (len(value) != 4))) or \
                (attr == 'iyr' and ((val_i < 2010 or val_i > 2020) or (len(value) != 4))) or \
                (attr == 'eyr' and ((val_i < 2020 or val_i > 2030) or (len(value) != 4))) or \
                (attr == 'hgt' and ((not re.search(r'^\d+(?:cm|in)(?!.)', value)) or ((re.search(r'^\d+cm(?!.)', value)) and (val_i < 150 or val_i > 193)) or ((re.search(r'^\d+in(?!.)', value)) and (val_i < 59 or val_i > 76)))) or \
                (attr == 'hcl' and (not re.search(r'^#[a-fA-F0-9]{6}(?!.)', value))) or \
                (attr == 'ecl' and (not re.search(r'^(?:amb|blu|brn|gry|grn|hzl|oth)(?!.)', value))) or \
                (attr == 'pid' and (not re.search(r'^\d{9}(?!.)', value))):
            # logging.debug(f'Attr {attr} had value {value} wrong')
            value = None
        self.__setattr__(attr, value)
        return value


with open(glob('*.input')[0]) as f:
    input_data = f.read()
del f
passports = []
passports_raw = input_data.split('\n\n')
del input_data
for passport_raw in passports_raw:
    passport = APASSPORT()
    for attr in iter(passport):
        if attr == 'ln':
            continue
        search = re.search(APASSPORT.regex_from_attr(attr), passport_raw)
        value = search.group(1) if search is not None else None
        if (value is not None) and (passport.set_value(attr, value) is not None) and (attr != 'cid'):
            passport.ln += 1
    passports.append(passport)

for passport in passports:
    if passport.ln == 7:
        for attr in iter(passport):
            if attr != 'cid' and attr != 'ln':
                logging.debug(f'{attr} = {getattr(passport, attr)}')  # if attr != "pid" else len(getattr(passport, attr))}')
        logging.debug('\n')

logging.debug(f'We found {len(passports)} passports')
logging.info(f'Among passports there were {sum([1 if x.ln == 7 else 0 for x in passports])} valid ones')