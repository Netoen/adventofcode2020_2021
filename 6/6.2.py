import logging
from glob import glob

logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

assert __name__ == "__main__", "You are not running the main file "

def read_input(sep: str = '\n') -> list:
    with open(glob('*.input')[0]) as f:
        input_data = f.read()
    del f
    return input_data.split(sep)


items = []
items_raw = read_input('\n\n')
for item in items_raw:
    items.append(item)
del items_raw
logging.debug(f'Currently list of seats looks like this:\n{items}')

group_summ = 0
for item in items:
    cur_group = []
    for line in item.split('\n'):
        cur_line = set()
        for char in line:
            cur_line.add(char)
        cur_group.append(cur_line)
    if len(cur_group) == 1:
        logging.info(f'cur_group is {len(cur_group[0])}')
        group_summ += len(cur_group[0])
        continue
    for char in cur_group[0] :
        found = True
        for future_line in cur_group[1:] :
            if not (char in future_line):
                found = False
                break
        group_summ += 1 if found else 0

logging.warning(f'Current groups summ is {group_summ}')
