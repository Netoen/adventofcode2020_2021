import logging
from glob import glob

logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

assert __name__ == "__main__", "You are not running the main file "

def read_input(sep: str = '\n') -> list:
    with open(glob('*.input')[0]) as f:
        input_data = f.read()
    del f
    return input_data.split(sep)


items = []
items_raw = read_input('\n\n')
for item in items_raw:
    items.append(item)
del items_raw
logging.debug(f'Currently list of seats looks like this:\n{items}')

group_summ = 0
for item in items:
    cur_group = set()
    for line in item.split('\n'):
        for char in line:
            cur_group.add(char)
    group_summ += len(cur_group)
logging.info(f'Current groups summ is {group_summ}')