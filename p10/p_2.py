import multiprocessing
from dataclasses import dataclass, field
if __name__ == '__main__':
    from os import path as os_path, getcwd as os_getcwd
    from sys import path as sys_path
    
    PACKAGE_PARENT = '..'
    SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
    sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
    import logging
    from lib import loader
    from functools import reduce
    logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')
    multiprocessing.freeze_support()


def main(**kwargs):
    puzzle = [int(number) for number in get_puzzle(kwargs)]
    solutions = [int(number) for number in get_solutions(kwargs)]

    puzzle.append(max(puzzle) + 3)
    puzzle.append(0)
    puzzle.sort()
    solution = spawn_children(puzzle)

    # solution.my_combinator(puzzle)
    # solution.s = purify(solution.s)

    if __name__ != '__main__':
        return puzzle, solutions
    else:
        logging.info(len(solution)+1)


def spawn_children(puzzle):
    subtasks = [puzzle[:idx] + puzzle[idx+1:]
                for idx in range(1, len(puzzle) - 2)
                if puzzle[idx + 1] - puzzle[idx - 1] <= 3]
    workers = 1
    workers = len(subtasks)
    with multiprocessing.Pool(workers) as pool:
        results = [pool.apply_async(my_combinator, (subtasks[idx],)) for idx in range(workers)]
        results = [result.get() for result in results]
        results = reduce(lambda x, y: x+y, results)
        _ = []
        for result in results:
            if result not in _:
                _.append(result)
        return _
            

def purify(solution):
    _ = []
    for item in solution:
        if item not in _:
            _.append(item)
    return _


def my_combinator(p: list, s: list = []) -> list:
    if not (_ := tuple(p)) in s:
        s.append(_)
        # print(len(s), end="\r")
    for idx in range(1, len(p)-2):
        if p[idx+1] - p[idx-1] <= 3 and not tuple(attempt := p[:idx]+p[idx+1:]) in s:
            s = my_combinator(attempt, s)
    return s


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '2_2.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


if __name__ == '__main__':
    main()
