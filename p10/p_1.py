from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
import logging
from lib import loader

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


def main(**kwargs):
    puzzle = [int(number) for number in get_puzzle(kwargs)]
    solutions = [int(number) for number in get_solutions(kwargs)]

    puzzle.append(max(puzzle)+3)
    puzzle.append(0)
    puzzle.sort()
    solution = {1:0, 2:0, 3:0}
    for number in map(lambda higher, lower: higher - lower, puzzle[1:], puzzle[:-1]):
        solution[number] += 1
    solution = solution[1] * solution[3]

    if __name__ != '__main__':
        return puzzle, solutions
    else:
        logging.info(solution)


def get_puzzle(kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    puzzle = loader.read_input(input_filename_puzzle, '\n')
    return puzzle


def get_solutions(kwargs):
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    return loader.read_input(input_filename_solutions, '\n')


main()
