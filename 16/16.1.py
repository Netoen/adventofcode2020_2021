from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path
PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
from pprint import pformat as pf
import logging
from lib import loader
from re import findall
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

def main():
    input_raw = loader.read_input('*.1.p', '\n')
    limits = []
    for line in input_raw:
        found = findall(r'(\d+)-(\d+)', line)
        for f in found:
            limits += [*range(int(f[0]), int(f[1])+1)]
        if 'your ticket:' in line:
            your_ticket = input_raw[input_raw.index(line) + 1]
        if 'nearby tickets:' in line:
            nearby_tickets_raw = input_raw[input_raw.index(line)+1 :]
            break
    
    summ = 0
    for ticket in nearby_tickets_raw:
        int_ticket = [int(number) for number in ticket.split(',')]
        for number in int_ticket:
            if number not in limits:
                summ += number

    logging.debug(pf(limits))
    logging.info(summ)

assert __name__ == "__main__", "You are not running the main file "

main()
