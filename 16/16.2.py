from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path, maxsize
PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
from pprint import pformat as pf
import logging
from lib import loader
from re import findall
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

CONST_SOLUTION_NAME = 'departure'

def main():
    input_raw = loader.read_input('*.1.p', '\n')
    limits, limits_all = [], []
    solution_idxs = []
    for line in input_raw:
        if CONST_SOLUTION_NAME in line:
            solution_idxs.append(input_raw.index(line))
        found = findall(r'(\d+)-(\d+)', line)
        _ = []
        for f in found:
            cur_range = [*range(int(f[0]), int(f[1])+1)]
            limits_all += cur_range
            _ += cur_range
        if _ != []:
            limits.insert(input_raw.index(line), _)
        if 'your ticket:' in line:
            your_ticket = [int(number) for number in input_raw[input_raw.index(line)+1].split(',')]
        if 'nearby tickets:' in line:
            nearby_tickets = [ [int(number) for number in ticket.split(',')] for ticket in input_raw[input_raw.index(line)+1 :] ]
            break
    del cur_range, f, found
    
    _ = nearby_tickets.copy()
    for ticket in _:
        for number in ticket:
            if number not in limits_all:
                nearby_tickets.remove(ticket)
    del _, number

    size = len(limits)
    solutions = [[*range(size)] for _ in range(size)]
    for ticket in nearby_tickets:
        for i in range(size):
            if len(solutions[i]) > 1:
                get_row(solutions, limits, ticket[i], i)
            if len(solutions[i]) == 1:
                weed_out(solutions, i)

    watchdog = 0
    while watchdog < 1000 or len(max(solutions, key=len)) > 1:
        watchdog += 1
        logging.debug(watchdog)
        for s in solutions:
            if len(s) == 1:
                weed_out(solutions, solutions.index(s))

    solutions = [s[0] for s in solutions]
    solution = 1
    for s in solution_idxs:
        solution *= your_ticket[solutions.index(s)]

    logging.debug(pf(limits_all))
    logging.info(solution)

def weed_out(solutions, i):
    for s in solutions:
        if (not s == solutions[i]) and (solutions[i][0] in s):
            solutions[solutions.index(s)].remove(solutions[i][0])

def get_row(solution, limits: list, number, pos: int) -> int:
    for line in limits:
        if number not in line:
            idx = limits.index(line)
            if idx in solution[pos]:
                solution[pos].remove(idx)


assert __name__ == "__main__", "You are not running the main file "

main()
