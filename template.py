from os import path as os_path, getcwd as os_getcwd
from sys import path as sys_path

PACKAGE_PARENT = '..'
SCRIPT_DIR = os_path.dirname(os_path.realpath(os_path.join(os_getcwd(), os_path.expanduser(__file__))))
sys_path.append(os_path.normpath(os_path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
import logging
from lib import loader

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


def main(**kwargs):
    input_filename_puzzle = kwargs['input_filename_puzzle'] if 'input_filename_puzzle' in kwargs.keys() else '*_1.p'
    input_filename_puzzle = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_puzzle
    input_filename_solutions = kwargs[
        'input_filename_solutions'] if 'input_filename_solutions' in kwargs.keys() else '*_1.s'
    input_filename_solutions = os_path.dirname(os_path.realpath(__file__)) + "\\" + input_filename_solutions
    input_raw = loader.read_input(input_filename_puzzle, '\n')
    solutions = loader.read_input(input_filename_solutions, '\n')


main()
