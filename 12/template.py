from dataclasses import dataclass
from glob import glob
import re
import logging
from math import sqrt, sin, cos, asin, acos, pi
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s -  %(levelname)s -  %(message)s')

with open(glob('12.1.input')[0]) as f:
    input_data = f.read().splitlines()
del f


def new_coords(x, y, delta: int) -> int:
    z = sqrt(x**2 + y**2)
    cur_angle = acos(x / z) * (-1 if (acos(x / z) * asin(y / z)) < 0 else 1)
    delta_rad = delta * pi / 180
    new_y = round(sin(cur_angle + delta_rad) * z)
    new_x = round(cos(cur_angle + delta_rad) * z)
    return new_x, new_y


# E or N => +, S or W => -
summ_x = summ_y = 0
d_x = 10
d_y = 1
regex = r'(\w)(\d+)'
ch_dir = {'L': +1, 'R': -1}
rose_x = {'E': +1, 'W': -1}
rose_y = {'N': +1, 'S': -1}
for instruction in input_data:
    key = re.search(regex, instruction).group(1)
    value = int(re.search(regex, instruction).group(2))
    if key in rose_x.keys():
        d_x += rose_x[key] * value
    elif key in rose_y.keys():
        d_y += rose_y[key] * value
    elif key in ch_dir.keys():
        d_x, d_y = new_coords(d_x, d_y, ch_dir[key] * value)
    elif key == 'F':
        summ_x += d_x * value
        summ_y += d_y * value
print(abs(summ_x) + abs(summ_y))
