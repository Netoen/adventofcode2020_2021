from glob import glob
from os import path


def read_input(filename: str = '*.p', sep: str = '\n') -> list:
    assert filename, 'Filename was not provided!'
    with open(glob(filename)[0]) as f:
        input_data = f.read()
    del f
    return input_data.split(sep)
