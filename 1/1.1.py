with open('1.1.input') as f:
    input_data = f.read().splitlines()
for line in input_data:
    # input_data[input_data.index(line)] = int(line)
    additive = 2020 - int(line)
    if str(additive) in input_data:
        print(additive * int(line))
pass