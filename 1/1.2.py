with open('1.1.input') as f:
    input_data = f.read().splitlines()
for line in input_data:
    # input_data[input_data.index(line)] = int(line)
    for line2 in input_data[input_data.index(line)+1:]:
        additive = 2020 - int(line) - int(line2)
        if str(additive) in input_data:
            print(additive * int(line) * int(line2))
pass