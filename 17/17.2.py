import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
import logging
from lib import loader
import numpy as np
from operator import add
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

# CONST
CONST_INACTIVE = '.'
CONST_ACTIVE = '#'
CONST_NUM_OF_DIMS = 4


def element_sum(a: list, b: tuple) -> tuple:
    return tuple(map(add, a, b))


def create_array(s, c=CONST_INACTIVE):
    _ = np.empty(shape=s, dtype='bytes_')
    _[:] = c
    return _


def expand_space(old_state):
    new_state = old_state.copy()
    for dim in range(new_state.ndim):
        # Expand current dim from "above"
        new_state = np.concatenate(
            (
                create_array(tuple([1 if dim == i else new_state.shape[i]
                             for i in range(new_state.ndim)])), new_state
            ), axis=dim)
        # Expand current dim from "below"
        new_state = np.concatenate(
            (
                new_state, create_array(
                    tuple([1 if dim == i else new_state.shape[i] for i in range(new_state.ndim)]))
            ), axis=dim)
    return new_state


def sum_neighbours(state: np.ndarray, idx: tuple, dt: list, dim: int = 0) -> int:
    local_summ = 0
    for cur_dt in range(-1, 2):
        if idx[dim] + cur_dt not in range(state.shape[dim]):
            continue
        dt[dim] = cur_dt
        if dim == state.ndim-1:
            # not the current cell itself
            if state[element_sum(dt, idx)] == CONST_ACTIVE and not (len(set(dt)) == 1 and 0 in set(dt)):
                local_summ += 1
        else:
            local_summ += sum_neighbours(state, idx, dt, dim+1)
    return local_summ


assert __name__ == "__main__", "You are not running the main file "

input_raw = loader.read_input('\n')

states = []
states.append(create_array((0, len(input_raw[0]))))
for row in input_raw:
    # TODO: maybe redo this with proper np creation
    states[0] = np.vstack([states[0], list(row)])
for dim in range(CONST_NUM_OF_DIMS-2):
    states[0] = np.expand_dims(states[0], axis=0)
for i in range(1, 7):
    states.append(expand_space(states[i-1]))
    wrk_state = states[i].copy()
    for idx, cube in np.ndenumerate(states[i]):
        cur_summ = sum_neighbours(states[i], idx, [-1]*CONST_NUM_OF_DIMS)
        if cube == CONST_ACTIVE and cur_summ not in (2, 3):
            wrk_state[idx] = CONST_INACTIVE
        elif cube == CONST_INACTIVE and cur_summ == 3:
            wrk_state[idx] = CONST_ACTIVE
    states[i] = wrk_state.copy()
solution = 0
solution = sum([1 for cube in np.nditer(states[-1]) if cube == CONST_ACTIVE])
logging.debug(states[-1])
logging.info(solution)
