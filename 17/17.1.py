import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from dataclasses import dataclass
import logging
from lib import loader
import numpy as np
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')

# CONST
CONST_INACTIVE = '.'
CONST_ACTIVE = '#'


def create_array(s, c=CONST_INACTIVE):
    _ = np.empty(shape=s, dtype='bytes_')
    _[:] = c
    return _


def expand_space(old_state):
    new_state = old_state.copy()
    # Third, X dimension expanse
    new_state = np.concatenate(
        (create_array((new_state.shape[0], new_state.shape[1], 1)), new_state), axis=2)
    new_state = np.concatenate((new_state, create_array(
        (new_state.shape[0], new_state.shape[1], 1))), axis=2)
    # Second, Y dimension expanse
    new_state = np.concatenate(
        (create_array((new_state.shape[0], 1, new_state.shape[2])), new_state), axis=1)
    new_state = np.concatenate((new_state, create_array(
        (new_state.shape[0], 1, new_state.shape[2]))), axis=1)
    # First, Z dimension expanse
    new_state = np.concatenate(
        (create_array((1, new_state.shape[1], new_state.shape[2])), new_state), axis=0)
    new_state = np.concatenate((new_state, create_array(
        (1, new_state.shape[1], new_state.shape[2]))), axis=0)
    return new_state


def sum_neighbours(state: np.ndarray, idx: tuple[int, int, int]) -> int:
    the_summ = 0
    for z in range(-1, 2):
        if z + idx[0] not in range(state.shape[0]):
            continue
        for y in range(-1, 2):
            if y + idx[1] not in range(state.shape[1]):
                continue
            for x in range(-1, 2):
                if x + idx[2] not in range(state.shape[2]):
                    continue
                if state[z+idx[0], y+idx[1], x+idx[2]] == CONST_ACTIVE and not (z == y == x == 0):  # not the current cell itself
                    the_summ += 1
    return the_summ


assert __name__ == "__main__", "You are not running the main file "

input_raw = loader.read_input('\n')

states = []
states.append(create_array((0, len(input_raw[0]))))
for row in input_raw:
    states[0] = np.vstack([states[0], list(row)])
states[0] = np.expand_dims(states[0], axis=0)
for i in range(1, 7):
    states.append(expand_space(states[i-1]))
    wrk_state = states[i].copy()
    for idx, cube in np.ndenumerate(states[i]):
        cur_summ = sum_neighbours(states[i], idx)
        if cube == CONST_ACTIVE and cur_summ not in (2, 3):
            wrk_state[idx] = CONST_INACTIVE
        elif cube == CONST_INACTIVE and cur_summ == 3:
            wrk_state[idx] = CONST_ACTIVE
    states[i] = wrk_state.copy()
solution = 0
solution = sum([1 for cube in np.nditer(states[-1]) if cube == CONST_ACTIVE])
logging.debug(states[-1])
logging.info(solution)
