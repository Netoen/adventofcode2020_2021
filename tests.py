import itertools as itto
import pprint


a = '0X10X01X1'
t = []
for ind in itto.product('01', repeat=3):
    ind = list(ind)
    t.append("".join([char if char != 'X' else ind.pop(0) for char in a]))
pprint.pprint(t)
