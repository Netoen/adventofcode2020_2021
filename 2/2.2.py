from dataclasses import dataclass
from glob import glob
import re


@dataclass
class a_password:
    limit_lower: int
    limit_upper: int
    letter: str
    pwd: str


with open(glob('*.input')[0]) as f:
    input_data = f.read().splitlines()
data = []
for line in input_data:
    # input_data[input_data.index(line)] =
    regex = re.search(r'(\d+)-(\d+) (\w): (\w+)', line)
    data.append(a_password(int(regex.group(1)), int(regex.group(2)), regex.group(3), regex.group(4)))
counter = 0
for password in data:
    # if password.limit_lower <= password.pwd.count(password.letter) <= password.limit_upper:
    if (password.pwd[password.limit_lower - 1] == password.letter) != (password.pwd[password.limit_upper - 1] == password.letter):
        print('{}-{} {}: {}'.format(str(password.limit_lower), password.limit_upper, password.letter, password.pwd))
        counter += 1
print(counter)
pass
