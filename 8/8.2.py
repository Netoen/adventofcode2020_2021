import os
import sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(
    os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from lib import loader
import logging
from dataclasses import dataclass
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


@dataclass
class a_line():
    cmd_name: str
    cmd_value: int
    used: bool = False


def iterate(array, care):
    local_callstack = []
    watchdog, cur_idx, summ = 0, 0, 0
    while (cur_idx < the_len) and ((not array[cur_idx].used) or not care) and (watchdog < 10000):
        watchdog += 1
        array[cur_idx].used = True
        if array[cur_idx].cmd_name in ('nop','jmp'):
            local_callstack.append(cur_idx)
        cur_pntr_name = array[cur_idx].cmd_name
        assert cur_pntr_name in 'nop acc jmp', f'The provided command {cur_pntr_name} is not one of the allowed ones'
        if cur_pntr_name == "nop":
            cur_idx += 1
        elif cur_pntr_name == "acc":
            summ += array[cur_idx].cmd_value
            cur_idx += 1
        elif cur_pntr_name == "jmp":
            cur_idx += array[cur_idx].cmd_value
    return watchdog, cur_idx, summ, local_callstack


assert __name__ == "__main__", "You are not running the main file"

input_raw = loader.read_input('\n')

array = []
for i in range(len(input_raw)):
    _ = input_raw[i].split(' ')
    array.append(a_line(_[0], int(_[1])))
logging.debug(f'Array consists of: \n{array}')

the_len = len(array)
_, _, _, callstack = iterate(array, True)

for _ in range(len(callstack)):
    changeling = callstack.pop()
    test_subject = array.copy()
    peaceful = False
    if array[changeling].cmd_name == 'nop':
        test_subject[changeling].cmd_name = 'jmp'
    elif array[changeling].cmd_name == 'jmp':
        test_subject[changeling].cmd_name = 'nop'
    else: 
        raise Exception('WE TRIED TO CHANGE A NON nop/jmp COMMAND!')
    watchdog_end, res_idx, summ, _ = iterate(test_subject, False)
    if res_idx == the_len:
        peaceful = True
        break

if watchdog_end >= 10000:
    logging.info(f'watchdog_end found slacking')
if res_idx == the_len + 1:
    logging.info(
        f'Something went wrong, res_idx is {res_idx} while the_len is {the_len}')
logging.info(f'And so the resulting value is {summ}')
