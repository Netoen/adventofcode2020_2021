import os
import sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(
    os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from lib import loader
import logging
from dataclasses import dataclass
logging.basicConfig(level=logging.INFO,
                    format=' %(asctime)s -  %(levelname)s -  %(message)s')


@dataclass
class a_line():
    cmd_name: str
    cmd_value: int
    used: bool = False


assert __name__ == "__main__", "You are not running the main file "

input_raw = loader.read_input('\n')

array = []
for i in range(len(input_raw)):
    _ = input_raw[i].split(' ')
    array.append(a_line(_[0], int(_[1])))
logging.debug(f'Array consists of: \n{array}')

watchdog, cur_idx, summ = 0, 0, 0
while not array[cur_idx].used:
    watchdog += 1
    array[cur_idx].used = True
    cur_pntr_name = array[cur_idx].cmd_name
    assert cur_pntr_name in 'nop acc jmp', f'The provided command {cur_pntr_name} is not one of the allowed ones'
    if cur_pntr_name == "nop":
        cur_idx += 1
    elif cur_pntr_name == "acc":
        summ += array[cur_idx].cmd_value
        cur_idx += 1
    elif cur_pntr_name == "jmp":
        cur_idx += array[cur_idx].cmd_value

logging.info(f'And so the resulting value is {summ}')